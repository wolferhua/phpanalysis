# 分词系统简介：
PHPAnalysis分词程序使用居于unicode的词库，使用反向匹配模式分词，理论上兼容编码更广泛，并且对utf-8编码尤为方便。 由于PHPAnalysis是无组件的系统，因此速度会比有组件的稍慢，不过在大量分词中，由于边分词边完成词库载入，因此内容越多，反而会觉得速度越快，这是正常现象，PHPAnalysis的词库是用一种类似哈希(Hash)的数据结构进行存储的，因此对于比较短的字符串分词，只需要占极小的资源，比那种一次性载入所有词条的实际性要高得多，并且词库容量大小不会影响分词执行的速度。
    PHPAnalysis分词系统是基于字符串匹配的分词方法进行分词的，这种方法又叫做机械分词方法，它是按照一定的策略将待分析的汉字串与 一个“充分大的”机器词典中的词条进行配，若在词典中找到某个字符串，则匹配成功（识别出一个词）。按照扫描方向的不同，串匹配分词方法可以分为正向匹配 和逆向匹配；按照不同长度优先匹配的情况，可以分为最大（最长）匹配和最小（最短）匹配；按照是否与词性标注过程相结合，又可以分为单纯分词方法和分词与 标注相结合的一体化方法。常用的几种机械分词方法如下：
1）正向最大匹配法（由左到右的方向）；
2）逆向最大匹配法（由右到左的方向）；
3）最少切分（使每一句中切出的词数最小）。
    还可以将上述各种方法相互组合，例如，可以将正向最大匹配方法和逆向最大匹配方法结合起来构成双向匹配法。由于汉语单字成词的特点，正向最小匹配和逆向 最小匹配一般很少使用。一般说来，逆向匹配的切分精度略高于正向匹配，遇到的歧义现象也较少。统计结果表明，单纯使用正向最大匹配的错误率为1/169， 单纯使用逆向最大匹配的错误率为1/245。但这种精度还远远不能满足实际的需要。实际使用的分词系统，都是把机械分词作为一种初分手段，还需通过利用各 种其它的语言信息来进一步提高切分的准确率。另一种方法是改进扫描方式，称为特征扫描或标志切分，优先在待分析字符串中识别和切分出一些带有明 显特征的词，以这些词作为断点，可将原字符串分为较小的串再来进机械分词，从而减少匹配的错误率。另一种方法是将分词和词类标注结合起来，利用丰富的词类 信息对分词决策提供帮助，并且在标注过程中又反过来对分词结果进行检验、调整，从而极大地提高切分的准确率。
    PHPAnalysis分词先对需要分词的词进行粗分，然后对粗分的短句子进行二次逆向最大匹配法(RMM)的方法进行分词，分词后对分词结果进行优化，然后才得到最终的分词结果。
# PHPAnalysis类API文档
### 一、比较重要的成员变量
```
$resultType   = 1;       // 生成的分词结果数据类型(1 为全部， 2为 词典词汇及单个中日韩简繁字符及英文， 3 为词典词汇及英文)这个变量一般用 SetResultType( $rstype ) 这方法进行设置。
$notSplitLen  = 5;        //切分句子最短长度
$toLower      = false;    //把英文单词全部转小写
$differMax    = false;    //使用最大切分模式对二元词进行消岐
$unitWord     = true;     //尝试合并单字(即是新词识别)
$differFreq   = false;    //使用热门词优先模式进行消岐
 
```
### 二、主要成员函数列表
#### 1、public function __construct($source_charset='utf-8', $target_charset='utf-8', $load_all=true, $source='') 
函数说明：构造函数
参数列表：
$source_charset      源字符串编码
$target_charset      目录字符串编码
$load_all            是否完全加载词典（此参数已经作废）
$source              源字符串
如果输入输出都是utf-8，实际上可以不必使用任何参数进行初始化，而是通过 SetSource 方法设置要操作的文本
#### 2、public function SetSource( $source, $source_charset='utf-8', $target_charset='utf-8' )
函数说明：设置源字符串
参数列表：
$source              源字符串
$source_charset      源字符串编码
$target_charset      目录字符串编码
返回值：bool
#### 3、public function StartAnalysis($optimize=true)
函数说明：开始执行分词操作
参数列表：
$optimize            分词后是否尝试优化结果
返回值：void
一个基本的分词过程：
//////////////////////////////////////
$pa = new PhpAnalysis();

$pa->SetSource('需要进行分词的字符串');

//设置分词属性
$pa->resultType = 2;
$pa->differMax  = true;

$pa->StartAnalysis();

//获取你想要的结果
$pa->GetFinallyIndex();
////////////////////////////////////////
#### 4、public function SetResultType( $rstype )
函数说明：设置返回结果的类型
实际是对成员变量$resultType的操作
参数 $rstype 值为：
1 为全部， 2为 词典词汇及单个中日韩简繁字符及英文， 3 为词典词汇及英文
返回值：void
#### 5、public function GetFinallyKeywords( $num = 10 )
函数说明：获取出现频率最高的指定词条数（通常用于提取文档关键字）
参数列表：
$num = 10  返回词条个数
返回值：用","分隔的关键字列表
#### 6、public function GetFinallyResult($spword=' ')
函数说明：获得最终分词结果
参数列表：
$spword    词条之间的分隔符
返回值：string
#### 7、public function GetSimpleResult()
函数说明：获得粗分结果
返回值：array
#### 8、public function GetSimpleResultAll()
函数说明：获得包含属性信息的粗分结果
属性（1中文词句、2 ANSI词汇（包括全角），3 ANSI标点符号（包括全角），4数字（包括全角），5 中文标点或无法识别字符）
返回值：array
#### 9、public function GetFinallyIndex()
函数说明：获取hash索引数组
返回值：array('word'=>count,...) 按出现频率排序
#### 10、public function MakeDict( $source_file, $target_file='' )
函数说明：把文本文件词库编译成词典
参数列表：
$source_file   源文本文件
$target_file   目标文件(如果不指定，则为当前词典)
返回值：void
#### 11、public function ExportDict( $targetfile )
函数说明：导出当前词典全部词条为文本文件
参数列表：
$targetfile  目标文件
返回值：void